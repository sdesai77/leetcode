/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    void inOrder(TreeNode* root, int k ,vector<int> &value){
        if (!root || value.size()>=k)
            return;
        inOrder(root->left, k, value);
        value.push_back(root->val);
        inOrder(root->right, k, value);
        return;
    }
    
    int kthSmallest(TreeNode* root, int k) {
        vector<int> value;
        inOrder(root, k, value);
        return value[k-1];
        
    }
};