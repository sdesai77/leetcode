//https://leetcode.com/problems/number-of-digit-one/

class Solution {
public:
    int countDigitOne(int n) {
        int nCount = 0;
        for (long long m = 1; m <= n; m *= 10){
            nCount += (n/m + 8) / 10 * m + (n/m % 10 == 1) * (n%m + 1);
        }
        return nCount;
    }
};
