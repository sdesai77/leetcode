//https://leetcode.com/problems/count-primes/

class Solution {
public:
    //function to check if a number is prime or not
    bool isPrime(int number){
        if(number < 2) return false;
        if(number == 2) return true;
        if(number % 2 == 0) return false;
        for(int i=3; (i*i)<=number; i+=2){
            if(number % i == 0 ) return false;
        }
        return true;

    }
    
    //function to count the number of prime numbers
    int countPrimes(int n) {
        int count = 0;
        for (int i=2;i<=n;i++){
            if(isPrime(i))
                count += 1;
            else
                continue;
        }
        return count;
    }

};