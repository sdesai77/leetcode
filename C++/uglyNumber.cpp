//https://leetcode.com/problems/ugly-number/
/*Divide the number by factor 2, 3 or 5 until
it cannot be done. If the remainder is 1, then
it is ugly number.*/


class Solution {
public:
    bool isUgly(int num) {
        if (num <= 0) return false;
        if (num == 1) return true;
        while (num > 1) {
            if (num % 2 == 0) {
                num /= 2;
                continue;
            }
            if (num % 3 == 0) {
                num /= 3;
                continue;
            }
            if (num % 5 == 0) {
                num /= 5;
                continue;
            }
            return false;
        }
        return true;
    }
};
