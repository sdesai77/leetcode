//https://leetcode.com/problems/binary-tree-zigzag-level-order-traversal/

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    vector<vector<int>> zigzagLevelOrder(TreeNode* root) {
        vector<vector<int>> result;
        if (!root) return result;
        queue<TreeNode *> q; // to Maintain the elements of the tree
        vector<int> level;
		q.push(root);
        q.push(NULL);   // End level indicator
        bool needReverse = true;    //flag to check if elements need to be reversed
        while (true){
            TreeNode *node = q.front(); q.pop();
            if (node){
                level.push_back(node->val);
                if (node->left) q.push(node->left);
                if (node->right) q.push(node->right);
            }
            else{
                if (!needReverse) 
                    reverse(level.begin(), level.end());
                result.push_back(level);
                level.clear();
                if (q.empty()) break;
                q.push(NULL);
                needReverse = !needReverse;
            }
        }
        return result;
    }
};