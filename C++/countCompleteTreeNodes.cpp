//https://leetcode.com/problems/count-complete-tree-nodes/

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    int findlDepth(TreeNode* root) {
        if (root==NULL) {
            return(0);
        } 
        else { 
        // compute the depth of each subtree on the left side
        return findlDepth(root->left)+1; 
        } 
        
    }
    
	int getDepth(TreeNode* root, int n, int bitCount){
        TreeNode* temp=root;
        for(int i=bitCount;i>0;i--)
            temp=(((1<<(i-1))&n)==0)?temp->left:temp->right;
            
        return(temp==NULL?bitCount:(bitCount+1));
	}
	
	
    int countNodes(TreeNode* root) {
        int leftDepth = findlDepth(root);	//max left depth
		if(leftDepth<2) return leftDepth;
		//if max depths are same then it is a full tree
		/*	For Example:
						 8
					   /   \
					 /       \
			    	4        12
				   / \       / \
				  /   \     /   \
				 2     6   10     6
				
				[ ]   [ ]  [ ]   [ ]
				
		*/
		int nMax = ((1<<leftDepth)-1);	//in a tree shown in the above example, total
										//number of nodes = (2^h)-1, where h=depth
		
		int N = (1<<(leftDepth-1));		//max number of nodes in the last level
										// = 2^(h-1)
		int left=0,right=N-1,mid=(left+right)/2;
		int bitCount=log2(N);
		
        int rightDepth = getDepth(root,N-1,bitCount);	//max right depth

		if(leftDepth==rightDepth) return nMax;
		
		//Traversing through the elements in the last row to find how many nodes are valid
		//For this we traverse from 1...N, binary search. getDepth()
		
		while(left < right){
			int midDepth = getDepth(root,mid,bitCount);
			
			if(midDepth==leftDepth) {
				left=mid+1;
				mid=(left+right)/2;
			}
			else{
				right=mid;
				mid=(left+right)/2;
			}
			
		}
		
		return(N-1+mid);
        /*if (root==NULL) return(0);
        else {
            return(countNodes(root->left)+countNodes(root->right)+1);
        }*/
    }
};