//https://leetcode.com/problems/maximum-subarray/

class Solution {
public:
    int maxSubArray(vector<int>& nums) {
        int max_sum=nums[0],i,sum=0;
        for(i=0;i<nums.size();i++){
            sum+=nums[i];
            max_sum=max(sum,max_sum);
            sum=max(sum,0);
        }
        return max_sum;
    }
};
