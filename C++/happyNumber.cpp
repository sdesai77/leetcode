//https://leetcode.com/problems/happy-number/

class Solution {
public:
    bool isHappy(int n) {
        if (n<0){
            return false;
        }
        int sum = 0;
        while (n!=0){
            sum += (n%10)*(n%10);
            n /=10;
        }
        if (sum==1) return true;
        else if (sum==4) return false;
        else isHappy(sum);
    }
};