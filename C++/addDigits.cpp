//https://leetcode.com/problems/add-digits/

class Solution {
public:
    int addDigits(int num) {
        if (num>=0 && num<=9){
            return num;
        }
        else{
            return (1+((num-1)%9));
        }
    }
};
