//https://leetcode.com/problems/majority-element/

class Solution {
public:
    int majorityElement(vector<int>& nums) {
        int counter=0, majElement;
        for (int i=0;i<nums.size();i++)
        {
            if (counter==0)
            {
                majElement = nums[i];
            }
            
            if(majElement==nums[i])
                counter++;
            else 
                counter-- ;
                
        }
        
        return majElement;
    }
};