//https://leetcode.com/problems/maximum-depth-of-binary-tree/

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    int maxDepth(TreeNode* root) {
        if (root==NULL) {
            return(0);
        } 
        else { 
    // compute the depth of each subtree 
        int lDepth = maxDepth(root->left); 
     //   int rDepth = maxDepth(root->right);
    // use the larger one 
     //   if (lDepth > rDepth) 
     return(lDepth+1); 
      //  else return(rDepth+1); 
        } 
        
    }
};