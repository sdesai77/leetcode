//https://leetcode.com/problems/4sum/

class Solution {
public:
    vector<vector<int>> fourSum(vector<int>& nums, int target) {
     	sort(nums.begin(), nums.end());
		set<vector<int>> res;
		int n = nums.size();
		for (int i = 0; i < n - 3; i++) {
			for (int j = i + 1; j < n - 2; j++) {
				int k = j + 1;
				int l = n - 1;

				while (k < l) {
					int sum = nums[i] + nums[j] + nums[k] + nums[l];
					if (sum == target) {
						vector<int> tmp(4);
						tmp[0] = nums[i];
						tmp[1] = nums[j];
						tmp[2] = nums[k];
						tmp[3] = nums[l];
						res.insert(tmp);
						k++;
						l--;
					} else if (sum < target) {
						k++;
					} else {
						l--;
					}
				}
			}
		}
		vector<vector<int>> result(res.begin(), res.end());
		return result;
    }
};
