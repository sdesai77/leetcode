//https://leetcode.com/problems/factorial-trailing-zeroes/

class Solution {
public:
    int trailingZeroes(int n) {
        if (n < 0)
		return -1;

    	
    	// Initialize result
        int count = 0;
     
        // Keep dividing n by powers of 5 and update count. A trailing zero is always
        // produced by prime factors 2 and 5. If we can count the number of 5s and 2s,
        // our task is done. Consider the following examples.
        // n = 5: 5!=5*4*3*2*1 = (5*(2*2)*3*2*1). So count of trailing 0s is 1.
        // n = 11: 11!=11*10*9*8*7*6*5*4*3*2*1 = 11*(5*2)*9*8*7*6*5*4*3*2*1. There are
        // two 5s prime factors of 11! . So count of trailing 0s is 2.
        
        for (long i=5; n/i>=1; i *= 5){
              count += n/i;
        }
        return count;
            
    }
};


